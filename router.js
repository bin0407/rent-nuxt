/* export function createRouter() {
  return new Router({
    mode: "history",
    routes: [
      {
        path: "/",
        component: MyPage
      }
    ]
  });
} */

//plugin part
import Vue from "vue";
import Router from "vue-router";
import goTo from "vuetify/es5/services/goto";

//rent part
import Home from "./pages/Home.vue";
import SearchRent from "./pages/SearchRent.vue";
import inspire from "./pages/inspire.vue";
/* import Driver from '../views/Driver.vue'
import CheckOutRent from '../views/CheckOutRent.vue' */

//travel part
/* import HomeTravel from '../views/HomeTravel.vue'
import SearchTravel from '../views/SearchTravel.vue'
import Travel from '../views/Travel.vue'
import CheckOutTravel from '../views/CheckOutTravel.vue' */

//genetal part
/* import Payment from '../views/Payment.vue'
import Success from '../views/Success.vue'
import Cancel from '../views/Cancel.vue'
import Help from '../views/Help.vue'
import Group from '../views/Group.vue' */

/* import Contact from '../views/Contact.vue'
import Map from '../views/Map.vue'
import Landing from '../views/Landing.vue' */
import NotFound from "./pages/404.vue";
/* 
import Meta from 'vue-meta'

import User from '../views/account/User.vue' */
/* import VueCookies from 'vue-cookies' */
/* import axios from 'axios' */

/* Vue.use(VueCookies) */

Vue.use(Router);
/* Vue.use(Meta) */
export function createRouter() {
  const routes = [
    //rent part
    {
      path: "/:lang(zh|ja|he|ar|ru|no|nl|sv|dk|pt|ca|it|de|fr|es|en)?",
      name: "Home",
      component: Home
    },
    {
      path: "*",
      name: "404",
      component: NotFound
    },
    {
      path:
        "/:lang(zh|ja|he|ar|ru|no|nl|sv|dk|pt|ca|it|de|fr|es|en)?/searchrent:baseUrl(/motorbike-rental-|/alquiler-motos-|/location-moto-|/moto-noleggio-|/motorrad-mieten-|/aluguer-motos-|/moto-lloguer-)?:city?",
      name: "SearchRent",
      component: SearchRent
    },
    {
      path: "/:lang(zh|ja|he|ar|ru|no|nl|sv|dk|pt|ca|it|de|fr|es|en)?/inspire",
      component: inspire,
      name: "inspire"
    },
    /* {
      path: '/:lang(zh|ja|he|ar|ru|no|nl|sv|dk|pt|ca|it|de|fr|es|en)?/driver',
      name: 'Driver',
      component: Driver
    },
    {
      path:
        '/:lang(zh|ja|he|ar|ru|no|nl|sv|dk|pt|ca|it|de|fr|es|en)?/checkoutrent',
      name: 'CheckOutRent',
      component: CheckOutRent
    }, */
    //travel part
    /* {
      path:
        '/:lang(zh|ja|he|ar|ru|no|nl|sv|dk|pt|ca|it|de|fr|es|en)?/hometravel',
      name: 'Hometravel',
      component: HomeTravel
    },
    {
      path:
        '/:lang(zh|ja|he|ar|ru|no|nl|sv|dk|pt|ca|it|de|fr|es|en)?/searchtravel',
      name: 'SearchTravel',
      component: SearchTravel
    },
    {
      path: '/:lang(zh|ja|he|ar|ru|no|nl|sv|dk|pt|ca|it|de|fr|es|en)?/travel',
      name: 'Travel',
      component: Travel
    },
    {
      path:
        '/:lang(zh|ja|he|ar|ru|no|nl|sv|dk|pt|ca|it|de|fr|es|en)?/checkouttravel',
      name: 'CheckOutTravel',
      component: CheckOutTravel
    }, */
    //genetal part
    /*  {
      path:
        '/:lang(zh|ja|he|ar|ru|no|nl|sv|dk|pt|ca|it|de|fr|es|en)?/user/:code',
      name: 'User',
      component: User
    },
    {
      path: '/:lang(zh|ja|he|ar|ru|no|nl|sv|dk|pt|ca|it|de|fr|es|en)?/payment',
      name: 'Payment',
      component: Payment
    },
    {
      path: '/:lang(zh|ja|he|ar|ru|no|nl|sv|dk|pt|ca|it|de|fr|es|en)?/contact',
      name: 'Contact',
      component: Contact
    },
    {
      path: '/:lang(zh|ja|he|ar|ru|no|nl|sv|dk|pt|ca|it|de|fr|es|en)?/help',
      name: 'Help',
      component: Help
    },
    {
      path:
        '/:lang(zh|ja|he|ar|ru|no|nl|sv|dk|pt|ca|it|de|fr|es|en)?/groups-reservations',
      name: 'Group',
      component: Group
    },
    {
      path:
        '/:lang(zh|ja|he|ar|ru|no|nl|sv|dk|pt|ca|it|de|fr|es|en)?/:baseUrl(motorbike-rental-|alquiler-motos-|location-moto-|moto-noleggio-|motorrad-mieten-|aluguer-motos-|moto-lloguer-):location',
      name: 'Landing',
      component: Landing
    },
    {
      path: '/:lang(zh|ja|he|ar|ru|no|nl|sv|dk|pt|ca|it|de|fr|es|en)?/success',
      name: 'Success',
      component: Success
    },
    {
      path: '/:lang(zh|ja|he|ar|ru|no|nl|sv|dk|pt|ca|it|de|fr|es|en)?/cancel',
      name: 'Cancel',
      component: Cancel
    },
    {
      path: '/:lang(zh|ja|he|ar|ru|no|nl|sv|dk|pt|ca|it|de|fr|es|en)?/map',
      name: 'Map',
      component: Map
    }, */
    { path: "*", redirect: "/" }
  ];

  const router = new Router({
    mode: "history",
    scrollBehavior: (to, from, savedPosition) => {
      let scrollTo = 0;

      if (to.hash) {
        scrollTo = to.hash;
      } else if (savedPosition) {
        scrollTo = savedPosition.y;
      }

      return goTo(scrollTo);
    },
    routes
  });

  router.beforeEach((to, from, next) => {
    let url = "";
    /* if (Vue.$cookies.isKey('currencies') !== true) {
      axios.post('/api/currency').then(res => {
        var now = new Date()
        var currencies = res.data.data
        var EffectDate = new Date(
          now.getFullYear() +
            ',' +
            parseInt(now.getMonth() + 1) +
            ',' +
            parseInt(now.getDate() + 1)
        )
        Vue.$cookies.set('currencies', currencies, EffectDate)
      })
    } */
    if (to.matched[0].name == "Landing") {
      let location = to.params.location;
      if (to.params.lang == undefined) {
        to.params.lang = "en";
      }
      if (to.params.baseUrl != "alquiler-motos-" && to.params.lang == "es") {
        next({ path: "/es/alquiler-motos-" + location });
      } else if (
        to.params.baseUrl != "location-moto-" &&
        to.params.lang == "fr"
      ) {
        next({ path: "/fr/location-moto-" + location });
      } else if (
        to.params.baseUrl != "moto-noleggio-" &&
        to.params.lang == "it"
      ) {
        next({ path: "/it/moto-noleggio-" + location });
      } else if (
        to.params.baseUrl != "motorrad-mieten-" &&
        to.params.lang == "de"
      ) {
        next({ path: "/de/motorrad-mieten-" + location });
      } else if (
        to.params.baseUrl != "moto-lloguer-" &&
        to.params.lang == "ca"
      ) {
        next({ path: "/ca/moto-lloguer-" + location });
      } else if (
        to.params.baseUrl != "motorbike-rental-" &&
        (to.params.lang == "en" || to.params.lang != undefined)
      ) {
        next({ path: "/motorbike-rental-" + location });
      } else {
        console.log("ss");
        next();
      }
    } else if (to.matched[0].name == "SearchRent") {
      let city = to.params.city;
      if (to.params.baseUrl != undefined) {
        if (to.params.lang == undefined) {
          to.params.lang = "en";
        }
        if (to.params.baseUrl != "alquiler-motos-" && to.params.lang == "es") {
          url = "alquiler-motos-";
          next({ path: "/searchrent/" + url + city });
        } else if (
          to.params.baseUrl != "location-moto-" &&
          to.params.lang == "fr"
        ) {
          url = "location-moto-";
          next({ path: "/searchrent/" + url + city });
        } else if (
          to.params.baseUrl != "moto-noleggio-" &&
          to.params.lang == "it"
        ) {
          url = "moto-noleggio-";
          next({ path: "/searchrent/" + url + city });
        } else if (
          to.params.baseUrl != "motorrad-mieten-" &&
          to.params.lang == "de"
        ) {
          url = "motorrad-mieten-";
          next({ path: "/searchrent/" + url + city });
        } else if (
          to.params.baseUrl != "moto-lloguer-" &&
          to.params.lang == "ca"
        ) {
          url = "moto-lloguer-";
          next({ path: "/searchrent/" + url + city });
        } else if (
          to.params.baseUrl != "motorbike-rental-" &&
          to.params.lang != "es" &&
          to.params.lang != "fr" &&
          to.params.lang != "it" &&
          to.params.lang != "de" &&
          to.params.lang != "ca"
        ) {
          url = "motorbike-rental-";
          next({ path: "/searchrent/" + url + city });
        } else {
          next();
        }
      } else {
        next();
      }
    }
    next();
  });

  return router;
}
