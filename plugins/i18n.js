import Vue from "vue";
import VueI18n from "vue-i18n";
Vue.use(VueI18n);

const dateTimeFormats = {
  en: {
    short: {
      year: "numeric",
      month: "2-digit",
      day: "2-digit"
    },
    long: {
      year: "numeric",
      month: "short",
      day: "numeric",
      //weekday: 'long',
      hour: "numeric",
      minute: "numeric"
    }
  },
  es: {
    short: {
      year: "numeric",
      month: "2-digit",
      day: "2-digit"
    },
    long: {
      year: "numeric",
      month: "short",
      day: "numeric",
      //weekday: 'long',
      hour: "numeric",
      minute: "numeric"
    }
  },
  de: {
    short: {
      year: "numeric",
      month: "2-digit",
      day: "2-digit"
    },
    long: {
      year: "numeric",
      month: "short",
      day: "numeric",
      //weekday: 'long',
      hour: "numeric",
      minute: "numeric"
    }
  },
  it: {
    short: {
      year: "numeric",
      month: "2-digit",
      day: "2-digit"
    },
    long: {
      year: "numeric",
      month: "short",
      day: "numeric",
      //weekday: 'long',
      hour: "numeric",
      minute: "numeric"
    }
  },
  ca: {
    short: {
      year: "numeric",
      month: "2-digit",
      day: "2-digit"
    },
    long: {
      year: "numeric",
      month: "short",
      day: "numeric",
      //weekday: 'long',
      hour: "numeric",
      minute: "numeric"
    }
  },
  pt: {
    short: {
      year: "numeric",
      month: "2-digit",
      day: "2-digit"
    },
    long: {
      year: "numeric",
      month: "short",
      day: "numeric",
      //weekday: 'long',
      hour: "numeric",
      minute: "numeric"
    }
  },
  da: {
    short: {
      year: "numeric",
      month: "2-digit",
      day: "2-digit"
    },
    long: {
      year: "numeric",
      month: "short",
      day: "numeric",
      //weekday: 'long',
      hour: "numeric",
      minute: "numeric"
    }
  },
  sv: {
    short: {
      year: "numeric",
      month: "2-digit",
      day: "2-digit"
    },
    long: {
      year: "numeric",
      month: "short",
      day: "numeric",
      //weekday: 'long',
      hour: "numeric",
      minute: "numeric"
    }
  },
  nl: {
    short: {
      year: "numeric",
      month: "2-digit",
      day: "2-digit"
    },
    long: {
      year: "numeric",
      month: "short",
      day: "numeric",
      //weekday: 'long',
      hour: "numeric",
      minute: "numeric"
    }
  },
  no: {
    short: {
      year: "numeric",
      month: "2-digit",
      day: "2-digit"
    },
    long: {
      year: "numeric",
      month: "short",
      day: "numeric",
      //weekday: 'long',
      hour: "numeric",
      minute: "numeric"
    }
  },
  ru: {
    short: {
      year: "numeric",
      month: "2-digit",
      day: "2-digit"
    },
    long: {
      year: "numeric",
      month: "short",
      day: "numeric",
      //weekday: 'long',
      hour: "numeric",
      minute: "numeric"
    }
  },
  ar: {
    short: {
      year: "numeric",
      month: "2-digit",
      day: "2-digit"
    },
    long: {
      year: "numeric",
      month: "short",
      day: "numeric",
      //weekday: 'long',
      hour: "numeric",
      minute: "numeric"
    }
  },
  he: {
    short: {
      year: "numeric",
      month: "2-digit",
      day: "2-digit"
    },
    long: {
      year: "numeric",
      month: "short",
      day: "numeric",
      //weekday: 'long',
      hour: "numeric",
      minute: "numeric"
    }
  },
  ja: {
    short: {
      year: "numeric",
      month: "short",
      day: "numeric"
    },
    long: {
      year: "numeric",
      month: "short",
      day: "numeric",
      weekday: "long",
      hour: "numeric",
      minute: "numeric",
      hour12: true
    }
  },
  zh: {
    short: {
      year: "numeric",
      month: "2-digit",
      day: "2-digit"
    },
    long: {
      year: "numeric",
      month: "short",
      day: "numeric",
      //weekday: 'long',
      hour: "numeric",
      minute: "numeric"
    }
  }
};

const numberFormats = {
  "en-US": {
    currency: {
      style: "currency",
      currency: "USD",
      currencyDisplay: "symbol"
    }
  },
  "en-AU": {
    currency: {
      style: "currency",
      currency: "AUD",
      currencyDisplay: "symbol"
    }
  },
  "en-EN": {
    currency: {
      style: "currency",
      currency: "GBP",
      currencyDisplay: "symbol"
    }
  },
  "es-ES": {
    currency: {
      style: "currency",
      currency: "EUR",
      currencyDisplay: "symbol"
    }
  },
  "es-AR": {
    currency: {
      style: "currency",
      currency: "ARS",
      currencyDisplay: "symbol"
    }
  },
  "pt-BR": {
    currency: {
      style: "currency",
      currency: "BRL",
      currencyDisplay: "symbol"
    }
  },
  "de-CH": {
    currency: {
      style: "currency",
      currency: "CHF",
      currencyDisplay: "symbol"
    }
  },
  "ja-JP": {
    currency: {
      style: "currency",
      currency: "JPY",
      currencyDisplay: "symbol"
    }
  }
};

export default ({ app, store }) => {
  app.i18n = new VueI18n({
    locale: store.state.locale,
    // set locale
    fallbackLocale: "en-US",
    // set fallback locale
    messages: {
      ar: require("@/assets/lang/ar.json"),
      ca: require("@/assets/lang/ca.json"),
      da: require("@/assets/lang/da.json"),
      de: require("@/assets/lang/de.json"),
      en: require("@/assets/lang/en.json"),
      es: require("@/assets/lang/es.json"),
      fr: require("@/assets/lang/fr.json"),
      he: require("@/assets/lang/he.json"),
      it: require("@/assets/lang/it.json"),
      ja: require("@/assets/lang/ja.json"),
      nl: require("@/assets/lang/nl.json"),
      no: require("@/assets/lang/no.json"),
      pt: require("@/assets/lang/pt.json"),
      ru: require("@/assets/lang/ru.json"),
      sv: require("@/assets/lang/sv.json"),
      zh: require("@/assets/lang/zh.json")
    },
    dateTimeFormats,
    numberFormats,
    silentTranslationWarn: true
  });
  app.i18n.path = link => {
    // 如果是默认语言，就省略
    if (app.i18n.locale === app.i18n.fallbackLocale) {
      return `/${link}`;
    }
    return `/${app.i18n.locale}/${link}`;
  };
};
