import colors from "vuetify/es5/util/colors";

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: "%s - rent-nuxt",
    title: "rent-nuxt",
    htmlAttrs: {
      lang: "en"
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ["@/plugins/i18n.js", { src: "~/plugins/store-cache", ssr: false }],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    "@nuxtjs/router",
    // https://go.nuxtjs.dev/vuetify
    "@nuxtjs/vuetify"
  ],
  render: {
    gzip: false
  },
  router: {
    extendRoutes(routes, resolve) {
      routes
        .push
        /*  {
          name: "notfound",
          path: "*",
          component: resolve(__dirname, "pages/404.vue")
        },

        {
          path: "/:lang(zh|ja|he|ar|ru|no|nl|sv|dk|pt|ca|it|de|fr|es|en)?",
          name: "Home",
          component: resolve(__dirname, "pages/_lang/index.vue")
        } */
        /* {
          path:
            "/:lang(zh|ja|he|ar|ru|no|nl|sv|dk|pt|ca|it|de|fr|es|en)?/:baseUrl(motorbike-rental-|alquiler-motos-|location-moto-|moto-noleggio-|motorrad-mieten-|aluguer-motos-|moto-lloguer-)?:city?",
          name: "SearchRent",
          component: resolve(__dirname, "pages/_lang/searchRent.vue")
        } */
        ();
    },
    middleware: ["device", "i18n"]
  },
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [],
  axios: {
    /* prefix: "/api",
    credentials: true,
    proxy: true */
  },
  proxy: {
    "/api": {
      target: "xxxx",
      pathRewrite: {
        "^/api/": "http://api.rentalmotorbike.com/"
      },

      changeOrigin: true
    }
  },
  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ["~/assets/variables.scss"]
    //treeShake: true
    /* defaultAssets: {
      font: {
        family: "Libre Baskerville"
      }
    },
    theme: {
      light: true,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    } */
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {}
};
