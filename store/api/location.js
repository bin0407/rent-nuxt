import axios from 'axios'

export function getCountriesDataApi() {
  return axios.post('/api/countries')
}

export function getCitiesDataApi() {
  return axios.post('/api/cities')
}
