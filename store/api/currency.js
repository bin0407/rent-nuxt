import axios from 'axios'

export function getCurrencyDataApi() {
  return axios.post('/api/currency')
}
