import axios from "axios";

export function getRecomendationsDataApi(locale, country) {
  console.log(country);
  return axios.post("/api/popularlocation", {
    locale: locale,
    country: country
  });
}
