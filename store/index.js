import { getCurrencyDataApi } from "./api/currency";
import { getCountriesDataApi, getCitiesDataApi } from "./api/location";
import { getRecomendationsDataApi } from "./api/home";

export const state = () => ({
  countries: [],
  cities: [],
  currency: "en-US",
  currencyData: [],
  acceptedCookies: false,
  chktoken: false,
  drawer: false,
  deviceType: "ss",
  recomendationsData: ["sss"],
  dicCurrency: {
    A: "en-AU",
    D: "en-US",
    E: "es-ES",
    G: "en-EN",
    Y: "ja-JP",
    Z: "af-ZA"
  },
  locale: "en",
  locales: [
    "zh",
    "ja",
    "ar",
    "ru",
    "no",
    "nl",
    "sv",
    "da",
    "pt",
    "ca",
    "it",
    "de",
    "fr",
    "es",
    "en"
  ],
  languageBox: {
    zh: {
      langLocale: "zh_CN",
      store_id: 15,
      lang: "zh_CN",
      clang: "/zh",
      currency: "en-US",
      code: "zh"
    },
    ja: {
      langLocale: "ja_JP",
      store_id: 14,
      lang: "ja_JP",
      clang: "/ja",
      currency: "ja-JP",
      code: "ja"
    },
    he: {
      langLocale: "he",
      store_id: 13,
      lang: "he",
      clang: "/he",
      currency: "en-US",
      code: "he"
    },
    ar: {
      langLocale: "ar_AE",
      store_id: 12,
      lang: "ar_AE",
      clang: "/ar",
      currency: "en-US",
      code: "ar"
    },
    ru: {
      langLocale: "ru_RU",
      store_id: 11,
      lang: "ru_RU",
      clang: "/ru",
      currency: "es-ES",
      code: "ru"
    },
    no: {
      langLocale: "no_NO",
      store_id: 10,
      lang: "no_NO",
      clang: "/no",
      currency: "es-ES",
      code: "no"
    },
    nl: {
      langLocale: "nl_NL",
      store_id: 9,
      lang: "nl_NL",
      clang: "/nl",
      currency: "es-ES",
      code: "nl"
    },
    sv: {
      langLocale: "sv_SE",
      store_id: 8,
      lang: "sv_SE",
      clang: "/sv",
      currency: "de-CH",
      code: "sv"
    },
    dk: {
      langLocale: "da_DK",
      store_id: 7,
      lang: "da_DK",
      clang: "/dk",
      currency: "es-ES",
      code: "dk"
    },
    pt: {
      langLocale: "pt_PT",
      store_id: 6,
      lang: "pt_PT",
      clang: "/pt",
      currency: "es-ES",
      code: "pt"
    },
    ca: {
      langLocale: "ca_CA",
      store_id: 5,
      lang: "ca_CA",
      clang: "/ca",
      currency: "es-ES",
      code: "ca"
    },
    it: {
      langLocale: "it_IT",
      store_id: 4,
      lang: "it_IT",
      clang: "/it",
      currency: "es-ES",
      code: "it"
    },
    de: {
      langLocale: "de_DE",
      store_id: 3,
      lang: "de_DE",
      clang: "/de",
      currency: "es-ES",
      code: "de"
    },
    fr: {
      langLocale: "fr_FR",
      store_id: 2,
      lang: "fr_FR",
      clang: "/fr",
      currency: "es-ES",
      code: "fr"
    },
    es: {
      langLocale: "es_ES",
      store_id: 1,
      lang: "es_ES",
      clang: "/es",
      currency: "es-ES",
      code: "es"
    },
    en: {
      langLocale: "en_US",
      store_id: 0,
      lang: "en_US",
      clang: "",
      currency: "en-US",
      code: "en"
    }
  }
});
export const getters = {
  getCurrencyData(state) {
    return state.currencyData;
  }
};
export const mutations = {
  SET_LANG(state, locale) {
    if (state.locales.indexOf(locale.locale) !== -1) {
      state.locale = locale.locale;
    }
  },
  // get all currency data and currency change
  setCurrencyData(state, payload) {
    state.currencyData = payload.currencyData;
  },
  // change current currency
  currencyChange(state, payload) {
    state.currency = payload.currency;
  },
  setCountriesData(state, payload) {
    state.countries = payload.countries;
  },
  setCitiesData(state, payload) {
    state.cities = payload.cities;
  },
  setDeviceType(state, payload) {
    state.deviceType = payload.type;
  },
  changeDrawer(state, payload) {
    state.drawer = payload.chk;
  },
  GET_RECOMENDATIONS(state, recomendations) {
    state.recomendations = recomendations;
  }
};

export const actions = {
  async setCurrencyData({ commit }) {
    const response = await getCurrencyDataApi();
    commit("setCurrencyData", { currencyData: response.data.data });
  },
  async currencyChange({ commit }, payload) {
    commit("currencyChange", { currency: payload.currency });
  },
  async setCountriesData({ commit }) {
    const response = await getCountriesDataApi();
    const countriesData = response.data.data;
    countriesData.sort((a, b) => (a.text > b.text ? 1 : -1));
    commit("setCountriesData", { countries: countriesData });
  },
  async setCitiesData({ commit }) {
    const response = await getCitiesDataApi();
    commit("setCitiesData", { cities: response.data.data });
  },
  async changeDrawer({ commit }, payload) {
    commit("changeDrawer", { chk: payload.chk });
  },
  async changeLocaleVuex({ commit }, payload) {
    commit("SET_LANG", { locale: payload.locale });
  },
  async fetchRecomendations({ commit, payload }) {
    const response = "aaaaaaaaaaaaaaaa";
    console.log(payload);
    commit(GET_RECOMENDATIONS, { locale: response });
  }
};
