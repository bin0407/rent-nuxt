// @ts-nocheck
import { deviceType } from "~/utils/deviceType";
export default function(context) {
  // @ts-ignore
  context.userAgent = process.server
    ? context.req.headers["user-agent"]
    : navigator.userAgent;
  // 给全局上下文添加一个属性来保存咱们返回的匹配信息
  context.deviceType = deviceType(context.userAgent);
  // 这里注入到store,是由于我部分页面须要判断机型请求不一样的数据,
  // 大家没有用到的话能够移除
  context.store.commit("setDeviceType", context.deviceType);
  // 如果判断UA非移动端的,就在这里作处理了..
  // context.redirect(status,url) 这个能够重定向到外部网站
  // 如果内部访问能够直接用router对象push
  return false;
  /* if (context.deviceType.type === "pc") { */
  // context.redirect(302,'www.huanjingwuyou.com')    //301是永久重定向，若是你想随着设备类型改变一直变，请改成302
  /* } else {
    context.redirect(302, "m.huanjingwuyou.com"); //301是永久重定向，若是你想随着设备类型改变一直变，请改成302
  } */
}
